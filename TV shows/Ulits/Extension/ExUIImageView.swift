//
//  ExUIImageView.swift
//  TV shows
//
//  Created by mac on 27/11/2020.
//

import Kingfisher

extension UIImageView {
    func setImage(with urlString: String){
        guard let url = URL.init(string: urlString) else {
            return
        }
        let resource = ImageResource(downloadURL: url, cacheKey: urlString)
        var kf = self.kf
        kf.indicatorType = .activity
        let defImage = #imageLiteral(resourceName: "logo")
        self.kf.setImage(with: resource, placeholder: defImage)
    }
}

