//
//  ExNotificationName.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import Foundation
extension Notification.Name{
    static let databaseUpdated = Notification.Name("databaseUpdated")
    static let favouritesUpdated = Notification.Name("favouritesUpdated")
    
    func post(center: NotificationCenter = NotificationCenter.default,
              object: Any? = nil,
              userInfo: [AnyHashable: Any]? = nil) {
      center.post(name: self, object: object, userInfo: userInfo)
    }

    func onPost(center: NotificationCenter = NotificationCenter.default,
                object: Any? = nil,
                queue: OperationQueue? = nil,
                using: @escaping (Notification) -> Void) -> NSObjectProtocol {
      return center.addObserver(forName: self, object: object, queue: queue, using: using)
    }
}
