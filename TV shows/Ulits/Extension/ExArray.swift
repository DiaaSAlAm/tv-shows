//
//  ExArray.swift
//  TV shows
//
//  Created by mac on 27/11/2020.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
