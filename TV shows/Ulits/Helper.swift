//
//  Helper.swift
//  TV shows
//
//  Created by mac on 27/11/2020.
//
 
 
public class Helper {
    
    /// Singletone instance
    public static let instance: Helper = Helper()
    
    // singletone
    private init(){}
    
    
    enum controllerID: String{
        case loginVC = "LoginVC"
    }
    
    enum storyboardName: String{
        case main = "Main" 
        
    }
    
    enum cells: String {
        case tvShowsCell = "TVShowsCell"
        case networkCell = "NetworkCell"
        case seasonsCell = "SeasonsCell"
        case recommendationsCell = "RecommendationsCell"
        case favouritesTVShowsCell = "FavouritesTVShowsCell"
    }
     
    
     
    
}



