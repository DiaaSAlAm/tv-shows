//
//  ConstantsColors.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

struct ConstantsColors {
    static let mainColor = UIColor(named: "mainColor")
    static let backgroundColor = UIColor(named: "backgroundColor")
}

