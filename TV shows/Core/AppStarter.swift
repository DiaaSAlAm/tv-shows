//
//  AppStarter.swift
//  TV shows
//
//  Created by mac on 27/11/2020.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    
    private init() {}
    
    func start(window: UIWindow?) {
        AppTheme.apply()
        setRootViewController(window: window)
    }
    
    private func setRootViewController(window: UIWindow?) {
        let rootViewController = UINavigationController(rootViewController: ListOfTVShowsRouter.createListOfTVShowsModule())
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}
