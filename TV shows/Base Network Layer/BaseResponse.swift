//
//  BaseResponse.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

class BaseResponse<T: Codable>: Codable {
    var success: Bool?
    var data: T?
    var message: String?
    var statusCode: Int?
    
    
    enum CodingKeys: String, CodingKey {
        case success
        case message = "status_message"
        case statusCode = "status_code"
    }
    
    
}
