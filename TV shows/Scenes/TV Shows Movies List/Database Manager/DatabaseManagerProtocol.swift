//
//  DatabaseManagerProtocol.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import Foundation

protocol DatabaseManagerProtocol {
  func add(favourite: FavouritesViewModel)
  func delete(favouriteId: Int, favouriteModel: FavouritesViewModel)
  func fetch() -> [FavouritesViewModel]
}
