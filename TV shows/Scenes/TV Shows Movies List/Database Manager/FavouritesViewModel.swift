//
//  FavouritesViewModel.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import Foundation
class FavouritesViewModel: Codable {
    var id: Int
    var name: String
    var voteAverage: String
    var voteCount: String
    var firstAirDate: String
    var posterPath: String
    init(id: Int,name: String, voteAverage: String, voteCount: String, firstAirDate: String,posterPath: String) {
        self.id = id
        self.name = name
        self.voteAverage = "\(voteAverage)%"
        self.voteCount = "\(voteCount)"
        self.firstAirDate = firstAirDate
        self.posterPath = (Environment.rootImagesURL.absoluteString) + (posterPath)
    }
}
