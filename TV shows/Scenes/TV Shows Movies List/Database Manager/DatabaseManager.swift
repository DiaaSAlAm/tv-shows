//
//  DatabaseManager.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import Foundation


class DatabaseManager: DatabaseManagerProtocol {
  
  static var shared: DatabaseManager = DatabaseManager()
  private let favouritesDB = FavouritesDB()
  
  private init() {}
  
    
    func add(favourite: FavouritesViewModel) {
        var favourites: [FavouritesViewModel] = favouritesDB.load()
 
        guard !favourites.contains(where: {$0.id == favourite.id}) else {
            delete(favouriteId: favourite.id, favouriteModel: favourite)
            return
          }
          
            favourites.append(favourite)
            saveInDB(favourites)
        }
  
  func delete(favouriteId: Int, favouriteModel: FavouritesViewModel) {
    var favourites: [FavouritesViewModel] = favouritesDB.load() 
        guard let index = favourites.firstIndex(where: { $0.id == favouriteModel.id }) else{return}
       favourites.remove(at: index)
       saveInDB(favourites)
  }
  
  func fetch() -> [FavouritesViewModel] {
     favouritesDB.load()
  }
  
  private func saveInDB(_ favourites: [FavouritesViewModel]) {
    favouritesDB.save(favourites) { status in
      if status {
        Notification.Name.databaseUpdated.post()
      } else {
        print("Couldn't save")
      }
    }
    
  }
}

class FavouritesDB {
  func load() -> [FavouritesViewModel] {
    guard let encodedData = UserDefaults.standard.array(forKey: UD.PrefKeys.favouritesViewModel) as? [Data] else {
        return []
    }
    let itemsModel = encodedData.map { try! JSONDecoder().decode(FavouritesViewModel.self, from: $0) }
    return itemsModel
  }
  
  func save<T>(_ items: [T], completion: @escaping (Bool) -> Void) where T: Codable {
    // TODO:- Add Saving method
    // 1- remove all old data
    // 2- add new data
    UserDefaults.standard.removeObject(forKey: UD.PrefKeys.favouritesViewModel)
    let data = items.map { try? JSONEncoder().encode($0) }
    UserDefaults.standard.set(data, forKey: UD.PrefKeys.favouritesViewModel)
    completion(true)
  }
}
