//
//  TVShowsListAPI.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit
protocol TVShowsAPIProtocol {
    
    func getSimilarTVShow(tvId: Int,completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void)
    
    func createGuestSessionId(completion: @escaping (Result<CreateGuestSessionModel?, NSError>) -> Void)
    
    func ratingTVShow(tvId: Int,guestSessionId: String, value: Int,completion: @escaping (Result<RatingTVShowsModel?, NSError>) -> Void)
    
    func getTVDetails(tvId: Int,completion: @escaping (Result<TVShowDetailsModel?, NSError>) -> Void)
    
    func getdiscoverTV(completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void)
}




class TVShowsAPI: BaseAPI<TVShowsNetwork>, TVShowsAPIProtocol {
    
    func getSimilarTVShow(tvId: Int, completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void) {
        self.fetchData(target: .getSimilarTVShow(tvId: tvId), responseClass: ListOfTVShowsModel.self, completion: completion)
    }
    
    func createGuestSessionId(completion: @escaping (Result<CreateGuestSessionModel?, NSError>) -> Void) {
        self.fetchData(target: .createGuestSession, responseClass: CreateGuestSessionModel.self, completion: completion)
    }
    
    
    func ratingTVShow(tvId: Int, guestSessionId: String, value: Int, completion: @escaping (Result<RatingTVShowsModel?, NSError>) -> Void) {
        self.fetchData(target: .rateing(tvId: tvId, guestSessionId: guestSessionId, value: value), responseClass: RatingTVShowsModel.self, completion: completion)
    }
    
    
    
    func getTVDetails(tvId: Int, completion: @escaping (Result<TVShowDetailsModel?, NSError>) -> Void) {
        self.fetchData(target: .getTVDetails(tvId: tvId), responseClass: TVShowDetailsModel.self, completion: completion)
    }
    
    func getdiscoverTV(completion: @escaping (Result<ListOfTVShowsModel?, NSError>) -> Void) {
        self.fetchData(target: .getdiscoverTV, responseClass: ListOfTVShowsModel.self, completion: completion)
    }
}
