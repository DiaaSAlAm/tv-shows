//
//  TVShowDetailsInteractor.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import Foundation

class TVShowDetailsInteractor: TVShowDetailsInteractorInputProtocol {
    
    //MARK: - Properties
    weak var presenter: TVShowDetailsInteractorOutputProtocol?
    private let tvShowsListNetwork: TVShowsAPIProtocol = TVShowsAPI() 
    
    
    func getTVDetails(tvId: Int) {
        tvShowsListNetwork.getTVDetails(tvId: tvId){ [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.presenter?.fetchingFailed(withError: ConstantsMessage.genericErrorMessage)
                default: 
                    self.presenter?.fetchedTVDetailsSuccessfuly(model: response!)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.presenter?.fetchingFailed(withError:ConstantsMessage.genericErrorMessage)
            }
        }
    }
    
    
    func ratingTVShow(tvId: Int, guestSessionId: String, value: Int) {
        tvShowsListNetwork.ratingTVShow(tvId: tvId, guestSessionId: guestSessionId, value: value){ [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.presenter?.fetchingFailed(withError: ConstantsMessage.genericErrorMessage)
                default:
                    self.presenter?.showMessage(message: response?.status_message ?? "")
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.presenter?.fetchingFailed(withError:ConstantsMessage.genericErrorMessage)
            }
        }
    }
    
    func createGuestSessionId_ratingTVShow(tvId: Int, value: Int) {
        tvShowsListNetwork.createGuestSessionId{ [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.presenter?.fetchingFailed(withError: ConstantsMessage.genericErrorMessage)
                default:
                    self.ratingTVShow(tvId: tvId, guestSessionId: response?.guest_session_id ?? "", value: value)
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.presenter?.fetchingFailed(withError:ConstantsMessage.genericErrorMessage)
            }
        }
    }
    
    
    func getSimilarTVShow(tvId: Int) {
        tvShowsListNetwork.getSimilarTVShow(tvId: tvId) { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.presenter?.fetchingFailed(withError: response?.message ?? ConstantsMessage.genericErrorMessage)
                default:
                    self.presenter?.fetchedSimilarTVShowsSuccessfuly(resultsListOfTVShows: response?.results ?? [])
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.presenter?.fetchingFailed(withError:ConstantsMessage.genericErrorMessage)
            }
        }
    }
     
     
}
