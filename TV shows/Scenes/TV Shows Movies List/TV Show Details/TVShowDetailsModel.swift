//
//  TVShowDetailsModel.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//
 
 
import Foundation

struct TVShowDetailsModel: Codable {
    let name : String?
    let overview : String?
    let genres : [Genres]?
    let vote_average : Double?
    let vote_count : Int?
    let poster_path : String?
    let networks : [Networks]?
    let homepage : String?
    let number_of_episodes : Int?
    let seasons : [Seasons]?
    let first_air_date: String?
    let id: Int?
} 

struct Genres: Codable {
    let id : Int?
    let name : String?
}

struct Networks: Codable {
    let name : String?
    let id : Int?
    let logo_path : String?
    let origin_country : String?
}

struct Seasons : Codable {
    let air_date : String?
    let episode_count : Int?
    let id : Int?
    let name : String?
    let overview : String?
    let poster_path : String?
    let season_number : Int?
}

struct ShowTVDetailsNetworksViewModel {
    var name: String?
    var logoPath: String
    
    init(data: Networks) {
        self.name = data.name
        self.logoPath = (Environment.rootImagesURL.absoluteString) + (data.logo_path ?? "")  
    }
}

struct ShowTVDetailsSeasonsViewModel {
    var name: String?
    var poster_path: String?
    var seasonNumber: String?
    
    init(data: Seasons) {
        self.name = data.name
        self.poster_path = (Environment.rootImagesURL.absoluteString) + (data.poster_path ?? "")
    }
}
