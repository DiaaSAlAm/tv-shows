//
//  TVShowDetailsPresenter.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

class TVShowDetailsPresenter: TVShowDetailsPresenterProtocol, TVShowDetailsInteractorOutputProtocol {
    
    
    //MARK: - Properties
    weak var view: TVShowDetailsViewProtocol?
    private let interactor: TVShowDetailsInteractorInputProtocol
    private var router: TVShowDetailsRouterProtocol
    private var tvId: Int
    var isFavorite: Bool
    private var showTVDetailsModel: TVShowDetailsModel?
    private var resultsListOfTVShows : [ResultsListOfTVShowsModel] = []
    var recommendationsNumberOfRows: Int { return resultsListOfTVShows.count }
    var networkNumberOfRow: Int {return showTVDetailsModel?.networks?.count ?? 0}
    var seasonsNumberOfRow: Int {return showTVDetailsModel?.seasons?.count ?? 0}
    var posterPathImage: String { return (Environment.rootImagesURL.absoluteString) + (showTVDetailsModel?.poster_path ?? "")   }
    var voteAverage: String {return "\(showTVDetailsModel?.vote_average ?? 0)%"}
    var showTitle: String {return showTVDetailsModel?.name ?? ""}
    var numberOfEpisodes: String {return "\(showTVDetailsModel?.number_of_episodes ?? 0) Episodes"}
    var overview: String {return showTVDetailsModel?.overview ?? ""}
    var homePageURL: String {return showTVDetailsModel?.homepage ?? ""}
    var genreName : String {
        return showTVDetailsModel?.genres?
            .compactMap { $0.name}.joined(separator: ", ") ?? ""
    }
    
    init(view: TVShowDetailsViewProtocol, interactor: TVShowDetailsInteractorInputProtocol, router: TVShowDetailsRouterProtocol,tvId: Int,isFavorite: Bool ) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.tvId = tvId
        self.isFavorite = isFavorite
    }
    
    func viewDidLoad() {
        view?.showLoadingIndicator()
        interactor.getTVDetails(tvId: tvId)
        interactor.getSimilarTVShow(tvId: tvId)
    }
    
    func fetchNextPage() {
        view?.showLoadingIndicator()
        interactor.getSimilarTVShow(tvId: tvId)
    }
    
    func createGuestSessionId_ratingTVShow(value: Int) {
        interactor.createGuestSessionId_ratingTVShow(tvId: tvId, value: value)
    }
    
    func fetchedTVDetailsSuccessfuly(model:TVShowDetailsModel) {
        self.showTVDetailsModel = model
        view?.hideLoadingIndicator()
        view?.successRequest()
    }
    
    func fetchedSimilarTVShowsSuccessfuly(resultsListOfTVShows: [ResultsListOfTVShowsModel]) {
        self.resultsListOfTVShows = resultsListOfTVShows
        view?.hideLoadingIndicator()
        view?.successRecommendationsRequest()
    }
    
    func fetchingFailed(withError error: String) { 
         view?.hideLoadingIndicator()
         view?.showMessage(message: error)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            guard let self = self else {return}
            self.view?.requestFailed()
        }
         
    }
    
    func showMessage(message: String) {
        if message != "" {
         view?.showMessage(message: message)
        }
    }
    
    func configureSeasonsCell(cell: TVShowDetailsSeasonsCellViewProtocol, indexPath: IndexPath) {
        guard let data = self.showTVDetailsModel?.seasons?.getElement(at: indexPath.row) else { return }
        let viewModel = ShowTVDetailsSeasonsViewModel(data: data)
        cell.configureSeasonsCell(viewModel: viewModel)
    }
    
    func configureNetworksCell(cell: TVShowDetailsNetworkCellViewProtocol, indexPath: IndexPath) {
        guard let data = self.showTVDetailsModel?.networks?.getElement(at: indexPath.row) else { return }
        let viewModel = ShowTVDetailsNetworksViewModel(data: data)
        cell.configureNetworksCell(viewModel: viewModel)
    }
    
    func configureRecommendationsCell(cell: TVShowDetailsRecommendationsCellViewProtocol, indexPath: IndexPath) {
        guard let data = self.resultsListOfTVShows.getElement(at: indexPath.row) else { return }
        let viewModel = ResultsListOfTVShowsViewModel(data: data)
        cell.configureRecommendationsCell(viewModel: viewModel)
    }
    
    func didTappedOnTVRow(indexPath: IndexPath){
        self.tvId = resultsListOfTVShows.getElement(at: indexPath.row)?.id ?? 0
        self.isFavorite = false
        self.viewDidLoad()
    }
    
    func didTappedFavorite(_ sender: UIButton){
        sender.isSelected = isFavorite
        sender.isSelected = !sender.isSelected 
        let isFavoriteImage = sender.isSelected ? UIImage(named: "favoriteFill") : UIImage(named: "favoriteEmpty")
        sender.setImage(isFavoriteImage, for: .normal)
        isFavorite = sender.isSelected
        Notification.Name.favouritesUpdated.post(userInfo: ["id": tvId, "favorite": isFavorite])
        addToDB()
    }
    
    func addToDB(){
        let favouritesViewModel = FavouritesViewModel(id: showTVDetailsModel?.id ?? 0, name: showTVDetailsModel?.name ?? "", voteAverage: "\(showTVDetailsModel?.vote_average ?? 0)", voteCount: "\(showTVDetailsModel?.vote_count ?? 0)", firstAirDate: showTVDetailsModel?.first_air_date ?? "", posterPath: showTVDetailsModel?.poster_path ?? "")
        DatabaseManager.shared.add(favourite: favouritesViewModel)
    }
}
