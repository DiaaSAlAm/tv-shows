//
//  RecommendationsCell.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

class RecommendationsCell: UICollectionViewCell, TVShowDetailsRecommendationsCellViewProtocol {

    //MARK: - IBOutlets
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var posterPathImage: UIImageView!
    @IBOutlet private weak var voteAverage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        posterPathImage.layer.cornerRadius = 10
    }
    
    func configureRecommendationsCell(viewModel: ResultsListOfTVShowsViewModel) {
        self.name.text = viewModel.name
        self.posterPathImage.setImage(with: viewModel.posterPath)
        self.voteAverage.text = viewModel.voteAverage
    }

}
