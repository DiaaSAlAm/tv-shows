//
//  NetworkCell.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

class NetworkCell: UICollectionViewCell, TVShowDetailsNetworkCellViewProtocol {
    
    //MARK: - IBOutlets 
    @IBOutlet private weak var logoPathImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureNetworksCell(viewModel: ShowTVDetailsNetworksViewModel) {
        self.logoPathImage.setImage(with: viewModel.logoPath)
    }
    
    

}
