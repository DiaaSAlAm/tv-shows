//
//  SeasonsCell.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

class SeasonsCell: UICollectionViewCell, TVShowDetailsSeasonsCellViewProtocol {
    
    
    //MARK: - IBOutlets
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var posterPathImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        posterPathImage.layer.cornerRadius = 8
    }
    
    func configureSeasonsCell(viewModel: ShowTVDetailsSeasonsViewModel) {
        self.name.text = viewModel.name
        self.posterPathImage.setImage(with: viewModel.poster_path ?? "")
    }

}
