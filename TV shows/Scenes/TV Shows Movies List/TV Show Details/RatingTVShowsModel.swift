//
//  RatingTVShowsModel.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import Foundation

struct RatingTVShowsModel: Codable {
    let success: Bool?
    let status_code: Int?
    let status_message: String?
} 
