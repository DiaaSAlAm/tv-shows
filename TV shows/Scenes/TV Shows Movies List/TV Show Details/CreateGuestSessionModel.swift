//
//  CreateGuestSessionModel.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import Foundation

struct CreateGuestSessionModel: Codable {
    let success: Bool?
    let guest_session_id: String?
}
