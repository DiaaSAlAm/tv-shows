//
//  TVShowDetails.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit
import SafariServices

protocol TVShowDetailsViewProtocol: class { //View Conteroller
    var presenter: TVShowDetailsPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func successRequest()
    func successRecommendationsRequest()
    func requestFailed()
    func showMessage(message: String)
}

protocol TVShowDetailsPresenterProtocol: class { // Logic
    var view: TVShowDetailsViewProtocol? { get set }
    func viewDidLoad()
    func configureNetworksCell(cell: TVShowDetailsNetworkCellViewProtocol, indexPath: IndexPath)
    func configureSeasonsCell(cell: TVShowDetailsSeasonsCellViewProtocol, indexPath: IndexPath)
    func configureRecommendationsCell(cell: TVShowDetailsRecommendationsCellViewProtocol,indexPath: IndexPath)
    func didTappedFavorite(_ sender: UIButton)
    func didTappedOnTVRow(indexPath: IndexPath)
    func createGuestSessionId_ratingTVShow(value: Int)
    var recommendationsNumberOfRows: Int {get}
    var networkNumberOfRow: Int {get}
    var seasonsNumberOfRow: Int {get}
    var voteAverage: String {get}
    var posterPathImage: String {get}
    var showTitle: String {get}
    var numberOfEpisodes: String {get}
    var overview: String {get}
    var genreName : String {get}
    var isFavorite: Bool {get}
    var homePageURL: String {get}
}

protocol TVShowDetailsInteractorInputProtocol: class { // func do it from presenter
     var presenter: TVShowDetailsInteractorOutputProtocol? { get set }
     func getTVDetails(tvId: Int) 
     func createGuestSessionId_ratingTVShow(tvId: Int, value: Int)
     func getSimilarTVShow(tvId: Int)
}

protocol TVShowDetailsInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedTVDetailsSuccessfuly(model:TVShowDetailsModel)
    func fetchedSimilarTVShowsSuccessfuly(resultsListOfTVShows : [ResultsListOfTVShowsModel])
    func fetchingFailed(withError error: String)
    func showMessage(message: String)
}

protocol TVShowDetailsRouterProtocol { // For Segue
     
}

protocol TVShowDetailsNetworkCellViewProtocol {
    func configureNetworksCell(viewModel: ShowTVDetailsNetworksViewModel)
}

protocol TVShowDetailsSeasonsCellViewProtocol {
    func configureSeasonsCell(viewModel: ShowTVDetailsSeasonsViewModel)
}

protocol TVShowDetailsRecommendationsCellViewProtocol {
    func configureRecommendationsCell(viewModel: ResultsListOfTVShowsViewModel)
}
