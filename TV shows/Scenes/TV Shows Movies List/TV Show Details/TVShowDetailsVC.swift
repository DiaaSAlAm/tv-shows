//
//  TVShowDetailsVC.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit
import Cosmos
import SafariServices

class TVShowDetailsVC: UIViewController, TVShowDetailsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var voteAverage: UILabel! 
    @IBOutlet private weak var posterPathImage: UIImageView!
    @IBOutlet private weak var rateView: CosmosView!
    @IBOutlet private weak var showTitle: UILabel!
    @IBOutlet private weak var showGenres: UILabel!
    @IBOutlet private weak var numberOfEpisodes: UILabel!
    @IBOutlet private weak var overview: UILabel!
    @IBOutlet private weak var homePageURL: UIButton!
    @IBOutlet private weak var networkCollectionView: UICollectionView!
    @IBOutlet private weak var networkViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var seasonsCollectionView: UICollectionView!
    @IBOutlet private weak var seasonsViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var recommendationsCollectionView: UICollectionView!
    @IBOutlet private weak var recommendationsViewHeight: NSLayoutConstraint!
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var favouriteButton: UIButton!
    //MARK: - Properties
    var presenter: TVShowDetailsPresenterProtocol!
    private let networkCell = Helper.cells.networkCell.rawValue
    private let seasonsCell = Helper.cells.seasonsCell.rawValue
    private let recommendationsCell = Helper.cells.recommendationsCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .gray)
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func showLoadingIndicator() {
        startAnimatingActivityView()
    }
    
    func hideLoadingIndicator() {
        stopAnimatingActivityView()
    }
    
    func successRequest() {
        setData()
        reloadData()
    }
    
    func requestFailed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func successRecommendationsRequest() {
        recommendationsCollectionView.reloadData()
        handelRecommendationsCollectionView()
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showError(message: message, view: self.view, backgroundColor: .black)
    }
    
    //MARK: IBAction
    @IBAction
    func didTappedFavorite(_ sender: UIButton){
        self.presenter.didTappedFavorite(sender)
    }
    
    @IBAction
    func didPressedUrlLinkButton(_ sender: UIButton) {
        openSafariViewController()
    }
     
}

//MARK: Setup View
extension TVShowDetailsVC {
    
     fileprivate func setupUI() {
        navigationController?.navigationBar.prefersLargeTitles = true
        posterPathImage.layer.cornerRadius = 10
        handelCollectionView()
        handelRecommendationsCollectionView()
        registerCollectionView()
        presenter.viewDidLoad()
        setupRatingView()
        
     }
    
    //MARK: setup Rating view
    fileprivate func setupRatingView(){
        rateView.didTouchCosmos = { [weak self] rating in
            guard let self = self else {return}
            self.presenter.createGuestSessionId_ratingTVShow(value: Int(rating))
         }
    }
    
    //MARK: - Start Animating Activity
    fileprivate func startAnimatingActivityView() {
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
    }
    
    //MARK: - Stop Animating Activity
    fileprivate func stopAnimatingActivityView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        networkCollectionView.delegate = self
        networkCollectionView.dataSource = self
        networkCollectionView.register(UINib(nibName: networkCell, bundle: nil), forCellWithReuseIdentifier: networkCell)
        
        seasonsCollectionView.delegate = self
        seasonsCollectionView.dataSource = self
        seasonsCollectionView.register(UINib(nibName: seasonsCell, bundle: nil), forCellWithReuseIdentifier: seasonsCell)
        
        recommendationsCollectionView.delegate = self
        recommendationsCollectionView.dataSource = self
        recommendationsCollectionView.register(UINib(nibName: recommendationsCell, bundle: nil), forCellWithReuseIdentifier: recommendationsCell)
    }
    
    fileprivate func setData(){
        self.posterPathImage.setImage(with: presenter.posterPathImage)
        self.voteAverage.text = presenter.voteAverage
        self.showTitle.text = presenter.showTitle
        self.title = presenter.showTitle
        self.numberOfEpisodes.text = presenter.numberOfEpisodes
        self.showGenres.text = presenter.genreName
        self.overview.text = presenter.overview
        self.homePageURL.setTitle(presenter.homePageURL, for: .normal)
        let isFavoriteImage = presenter.isFavorite ? UIImage(named: "favoriteFill") : UIImage(named: "favoriteEmpty")
        favouriteButton.setImage(isFavoriteImage, for: .normal)
    }
    
    fileprivate func reloadData(){
        rateView.rating = 0
        networkCollectionView.reloadData()
        seasonsCollectionView.reloadData()
        handelCollectionView()
    }
    
    fileprivate func handelRecommendationsCollectionView(){
        recommendationsViewHeight.constant = presenter.recommendationsNumberOfRows == 0 ? 0 : 350
    }
    
    fileprivate func handelCollectionView(){
        self.networkViewHeight.constant = presenter.networkNumberOfRow == 0 ? 0 : 150
        self.seasonsViewHeight.constant = presenter.seasonsNumberOfRow == 0 ? 0 : 225
       
    }
    
    fileprivate func openSafariViewController(){
        guard let url = URL(string: presenter.homePageURL) else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
}

 
//MARK: - UICollectionView Delegate
extension TVShowDetailsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == recommendationsCollectionView {
            let topOffset = CGPoint(x: 0, y: 0)
            scrollView.setContentOffset(topOffset, animated: true)
            presenter.didTappedOnTVRow(indexPath: indexPath)
        }
        
    }
}

//MARK: - UICollectionView Data Source
extension TVShowDetailsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case networkCollectionView:
            print("presenter.networkNumberOfRow", presenter.networkNumberOfRow)
            return presenter.networkNumberOfRow
        case seasonsCollectionView:
            print("presenter.seasonsNumberOfRow", presenter.seasonsNumberOfRow)
            return presenter.seasonsNumberOfRow
        default:
            print("presenter.recommendationsNumberOfRows", presenter.recommendationsNumberOfRows)
            return presenter.recommendationsNumberOfRows
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
         case networkCollectionView:
             let cell = networkCollectionView.dequeueReusableCell(withReuseIdentifier: networkCell,for: indexPath) as! NetworkCell
               presenter.configureNetworksCell(cell: cell, indexPath: indexPath)
             return cell
         case seasonsCollectionView:
             let cell = seasonsCollectionView.dequeueReusableCell(withReuseIdentifier: seasonsCell,for: indexPath) as! SeasonsCell
             presenter.configureSeasonsCell(cell: cell, indexPath: indexPath)
            return cell
         default:
            let cell = recommendationsCollectionView.dequeueReusableCell(withReuseIdentifier: recommendationsCell,for: indexPath) as! RecommendationsCell
            presenter.configureRecommendationsCell(cell: cell, indexPath: indexPath)
            return cell
         }
       }
}

//MARK: - UICollectionView Delegate Flow Layout
extension TVShowDetailsVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case networkCollectionView:
            let width = self.networkCollectionView.frame.width
            let height = self.networkCollectionView.frame.height
            return CGSize(width: width / 5 , height: height - 24)
        case seasonsCollectionView:
            let width = self.seasonsCollectionView.frame.width
            let height = self.seasonsCollectionView.frame.height
            return CGSize(width: (width / 3) , height: height - 24)
        default:
            let width = self.recommendationsCollectionView.frame.width
            let height = self.recommendationsCollectionView.frame.width
            return CGSize(width: (width / 2) , height: height - 32)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell horizonatally
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell vertically
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        // give space top left bottom and right for cells
        return UIEdgeInsets(top: 10, left: 0 , bottom: 10, right: 0)
    } 
}
