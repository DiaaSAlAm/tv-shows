//
//  MyFavouritesRouter.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import UIKit
 
class MyFavouritesRouter: MyFavouritesRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    func goToShowTVDetails(tvId: Int) -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(TVShowDetailsVC.self)") as! TVShowDetailsVC
        let interactor = TVShowDetailsInteractor()
        let router = TVShowDetailsRouter()
        let presenter = TVShowDetailsPresenter(view: view, interactor: interactor, router: router, tvId: tvId, isFavorite: true)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    

}
