//
//  MyFavouritesPresenter.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import UIKit
 

class MyFavouritesPresenter: MyFavouritesPresenterProtocol, MyFavouritesInteractorOutputProtocol { 
    
    //MARK: - Properties
    weak var view: MyFavouritesViewProtocol?
    private let interactor: MyFavouritesInteractorInputProtocol
    private var router: MyFavouritesRouterProtocol
    private var favouritesTVShowList = DatabaseManager.shared.fetch()
    var numberOfRows: Int {
        return favouritesTVShowList.count
    }
    
    init(view: MyFavouritesViewProtocol, interactor: MyFavouritesInteractorInputProtocol, router: MyFavouritesRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    } 
    
    func viewDidLoad() {
         favoriteNotificationListener()
         self.view?.reloadData()
    }
     
    
    func configureCell(cell: MyFavouritesCellViewProtocol, indexPath: IndexPath) {
        guard let viewModel = self.favouritesTVShowList.getElement(at: indexPath.row) else { return }
        cell.configureCell(viewModel: viewModel, indexPath: indexPath)
    }
    
    func didTappedOnTVRow(indexPath: IndexPath) -> UIViewController {
         let data = favouritesTVShowList.getElement(at: indexPath.row)
        return router.goToShowTVDetails(tvId: data?.id ?? 0)
    }
    
    func removeFavorite(_ row: Int) {
        updateFavoriteSelected(row)
        removeFavoritesFromDB(row)
    }
    
    func removeFavoritesFromDB(_ row: Int){
        guard let data = self.favouritesTVShowList.getElement(at: row) else {return }
        DatabaseManager.shared.add(favourite: data)
    }
    
    func updateFavoriteSelected(_ row: Int){
        let tvId = favouritesTVShowList.getElement(at:row)?.id ?? 0
        Notification.Name.favouritesUpdated.post(userInfo: ["id": tvId, "favorite": false])
    }
    
    fileprivate func favoriteNotificationListener() {
        _ = Notification.Name.databaseUpdated.onPost { [weak self] (notification) in
            guard let self = self else {return}
            self.favouritesTVShowList = DatabaseManager.shared.fetch()
            self.view?.reloadData()
        }
    }
     
}

