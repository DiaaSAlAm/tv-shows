//
//  FavouritesTVShowsCell.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import UIKit
 
class FavouritesTVShowsCell: UICollectionViewCell, MyFavouritesCellViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var name: UILabel!
    @IBOutlet private weak var voteAverage: UILabel!
    @IBOutlet private weak var firstAirDate: UILabel!
    @IBOutlet private weak var posterPathImage: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    
    //MARK: - Properties
    
    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        posterPathImage.layer.cornerRadius = 8
    }
    
    //MARK: - Configure Cell
    func configureCell(viewModel: FavouritesViewModel, indexPath: IndexPath) {
        self.name.text = viewModel.name
        self.voteAverage.text = viewModel.voteAverage
        self.firstAirDate.text = viewModel.firstAirDate
        self.posterPathImage.setImage(with: viewModel.posterPath)
    }

}
