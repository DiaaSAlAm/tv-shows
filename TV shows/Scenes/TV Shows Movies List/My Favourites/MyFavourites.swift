//
//  MyFavourites.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import UIKit

protocol MyFavouritesViewProtocol: class { //View Conteroller
    var presenter: MyFavouritesPresenterProtocol! { get set } 
    func reloadData()
    func showMessage(message: String)
}

protocol MyFavouritesPresenterProtocol: class { // Logic
    var view: MyFavouritesViewProtocol? { get set }
    func viewDidLoad()
    func configureCell(cell: MyFavouritesCellViewProtocol, indexPath: IndexPath)
    func didTappedOnTVRow(indexPath: IndexPath) -> UIViewController
    func removeFavorite(_ row: Int) 
    var numberOfRows: Int {get}
}

protocol MyFavouritesInteractorInputProtocol: class { // func do it from presenter
     var presenter: MyFavouritesInteractorOutputProtocol? { get set }
    
}

protocol MyFavouritesInteractorOutputProtocol: class { // it's will call when interactor finished 
}

protocol MyFavouritesRouterProtocol { // For Segue
    func goToShowTVDetails(tvId: Int) -> UIViewController
    
}

protocol MyFavouritesCellViewProtocol{ // For Segue
    func configureCell(viewModel: FavouritesViewModel, indexPath: IndexPath)
}
