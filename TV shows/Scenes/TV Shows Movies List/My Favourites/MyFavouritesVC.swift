//
//  MyFavouritesVC.swift
//  TV shows
//
//  Created by mac on 29/11/2020.
//

import UIKit
 
class MyFavouritesVC: UIViewController, MyFavouritesViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: MyFavouritesPresenterProtocol!
    private let cellIdentifier = Helper.cells.favouritesTVShowsCell.rawValue
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    } 
    
    func reloadData() {
        handleCollectionState()
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showError(message: message, view: self.view, backgroundColor: .black)
    }
    
    @objc func didTappedFavorite(sender:UIButton!){
        let row = sender.tag
        presenter.removeFavorite(row)
    }
    
    //MARK: - IBOutlets
}

//MARK: Setup View
extension MyFavouritesVC {
    
     fileprivate func setupUI() {
        title = "My Favourites" 
        registerCollectionView()
        presenter.viewDidLoad()
     }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - handle label on background collectionView
    func handleCollectionState() {
        if presenter.numberOfRows == 0 {
            let frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 24, height: 30)
            let label = UILabel(frame: frame)
            label.text = "No Favourites Found"
            label.textColor = .darkGray
            label.textAlignment = .center
            label.center = collectionView.center
            label.sizeToFit()
            collectionView.backgroundView = label
        } else {
            collectionView.backgroundView = nil 
        }
        collectionView.reloadData()
    }
}

 
//MARK: - UICollectionView Delegate
extension MyFavouritesVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let view = presenter.didTappedOnTVRow(indexPath: indexPath)
        self.navigationController?.pushViewController(view, animated: true)
         
    }
}

//MARK: - UICollectionView Data Source
extension MyFavouritesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! FavouritesTVShowsCell
        cell.favouriteButton.addTarget(self, action: #selector(self.didTappedFavorite(sender:)), for: .touchUpInside)
        cell.favouriteButton.tag = indexPath.row 
        presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension MyFavouritesVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width
        return CGSize(width: (width / 2) , height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell horizonatally
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell vertically
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        // give space top left bottom and right for cells
        return UIEdgeInsets(top: 10, left: 0 , bottom: 10, right: 0)
    }
}
