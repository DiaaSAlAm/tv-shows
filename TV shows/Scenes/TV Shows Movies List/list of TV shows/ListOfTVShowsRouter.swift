//
//  ListOfTVShowsRouter.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit
 
class ListOfTVShowsRouter: ListOfTVShowsRouterProtocol {
    //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createListOfTVShowsModule() -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(ListOfTVShowsVC.self)") as! ListOfTVShowsVC
        let interactor = ListOfTVShowsInteractor()
        let router = ListOfTVShowsRouter()
        let presenter = ListOfTVShowsPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func goToShowTVDetails(tvId: Int, isFavorite: Bool) -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(TVShowDetailsVC.self)") as! TVShowDetailsVC
        let interactor = TVShowDetailsInteractor()
        let router = TVShowDetailsRouter()
        let presenter = TVShowDetailsPresenter(view: view, interactor: interactor, router: router, tvId: tvId, isFavorite: isFavorite)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func goToMyFavourites() -> UIViewController {
        let view = UIStoryboard(name: Helper.storyboardName.main.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(MyFavouritesVC.self)") as! MyFavouritesVC
        let interactor = MyFavouritesInteractor()
        let router = MyFavouritesRouter()
        let presenter = MyFavouritesPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }

}
