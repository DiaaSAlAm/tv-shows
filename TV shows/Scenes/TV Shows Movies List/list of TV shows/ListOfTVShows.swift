//
//  ListOfTVShows.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

protocol ListOfTVShowsViewProtocol: class { //View Conteroller
    var presenter: ListOfTVShowsPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func reloadData()
    func showMessage(message: String)
}

protocol ListOfTVShowsPresenterProtocol: class { // Logic
    var view: ListOfTVShowsViewProtocol? { get set }
    var numberOfRows: Int {get}
    func viewDidLoad()
    func configureCell(cell: ListOfTVShowsCellViewProtocol, indexPath: IndexPath)
    func didTappedOnTVRow(indexPath: IndexPath) -> UIViewController
    func didTappedFavorite(_ row: Int, _ isFavourite: Bool)
    func getIsFavorite(_ row: Int) -> Bool
    func goToMyFavourites() -> UIViewController
    
}

protocol ListOfTVShowsInteractorInputProtocol: class { // func do it from presenter
     var presenter: ListOfTVShowsInteractorOutputProtocol? { get set }
    func getdiscoverTV()
}

protocol ListOfTVShowsInteractorOutputProtocol: class { // it's will call when interactor finished
    func fetchedSuccessfuly(resultsListOfTVShows : [ResultsListOfTVShowsModel])
    func fetchingFailed(withError error: String) 
}

protocol ListOfTVShowsRouterProtocol { // For Segue
    func goToShowTVDetails(tvId: Int, isFavorite: Bool) -> UIViewController
    func goToMyFavourites() -> UIViewController
}

protocol ListOfTVShowsCellViewProtocol {
    func configureCell(viewModel: ResultsListOfTVShowsViewModel, indexPath: IndexPath)
}
