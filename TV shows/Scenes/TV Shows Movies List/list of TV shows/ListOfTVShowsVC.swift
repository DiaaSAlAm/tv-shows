//
//  ListOfTVShowsVC.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit
 
class ListOfTVShowsVC: UIViewController, ListOfTVShowsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: ListOfTVShowsPresenterProtocol!
    private let cellIdentifier = Helper.cells.tvShowsCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .white)
    private let fadeView: UIView = UIView()
    let refreshController = UIRefreshControl()
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func showLoadingIndicator() {
        startAnimatingActivityView()
    }
    
    func hideLoadingIndicator() {
        self.refreshController.endRefreshing()
        stopAnimatingActivityView()
    }
    
    func reloadData() {
        handleCollectionState()
    }
    
    func showMessage(message: String) {
        ToastManager.shared.showError(message: message, view: self.view, backgroundColor: .black)
    }
    
    //MARK: - @objc
    @objc func didTappedFavorite(sender:UIButton!){
        let row = sender.tag
        let isFavorite = sender.isSelected ? false : true
        sender.isSelected = isFavorite
        let isFavoriteImage = sender.isSelected ? UIImage(named: "favoriteFill") : UIImage(named: "favoriteEmpty")
        sender.setImage(isFavoriteImage, for: .normal)
        presenter.didTappedFavorite(row, isFavorite)
    }
    
    @objc func refreshUserList(){
        presenter.viewDidLoad()
    }
    
    //MARK: -@IBOutlets
    @IBAction
    func didTappedFavouriteBarItem(_ sender: UIBarButtonItem) {
        let view = presenter.goToMyFavourites()
        self.navigationController?.pushViewController(view, animated: true)
    }
}

//MARK: Setup View
extension ListOfTVShowsVC {
    
     fileprivate func setupUI() {
        title = "List Of TV Shows" 
        registerCollectionView()
        presenter.viewDidLoad()
        setupRefreshControl()
     }
    
    //MARK: - Start Animating Activity
    fileprivate func startAnimatingActivityView() {
        
        fadeView.frame = self.view.frame
        fadeView.backgroundColor = .black
        fadeView.alpha = 0.4
        self.view.addSubview(fadeView)

        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
    }
    
    //MARK: - Stop Animating Activity
    fileprivate func stopAnimatingActivityView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.fadeView.removeFromSuperview()
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - handle label on background collectionView
    func handleCollectionState() {
        if presenter.numberOfRows == 0 {
            let frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 24, height: 30)
            let label = UILabel(frame: frame)
            label.text = "No Data Available"
            label.textColor = .darkGray
            label.textAlignment = .center
            label.center = collectionView.center
            label.sizeToFit()
            collectionView.backgroundView = label
        } else {
            collectionView.backgroundView = nil
        }
        collectionView.reloadData()
    }
    
    fileprivate func setupRefreshControl(){
        collectionView.refreshControl = refreshController
        refreshController.addTarget(self, action: #selector(refreshUserList), for: .valueChanged)
        refreshController.tintColor = .gray
        refreshController.layer.zPosition = -1
        collectionView.refreshControl = refreshController
        self.collectionView.showsVerticalScrollIndicator = false
    }
}

 
//MARK: - UICollectionView Delegate
extension ListOfTVShowsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let view = presenter.didTappedOnTVRow(indexPath: indexPath)
        self.navigationController?.pushViewController(view, animated: true)
         
    }
}

//MARK: - UICollectionView Data Source
extension ListOfTVShowsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! TVShowsCell
        let isFavourite = presenter.getIsFavorite(indexPath.row)
        let image = isFavourite ? UIImage(named: "favoriteFill") : UIImage(named: "favoriteEmpty")
        cell.favouriteButton.setImage(image, for: .normal)
        cell.favouriteButton.addTarget(self, action: #selector(self.didTappedFavorite(sender:)), for: .touchUpInside)
        cell.favouriteButton.tag = indexPath.row
        cell.favouriteButton.isSelected = isFavourite
        presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension ListOfTVShowsVC: UICollectionViewDelegateFlowLayout { 
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width
        return CGSize(width: (width / 2) , height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell horizonatally
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        // splace between two cell vertically
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets{
        // give space top left bottom and right for cells
        return UIEdgeInsets(top: 10, left: 0 , bottom: 10, right: 0)
    } 
}
