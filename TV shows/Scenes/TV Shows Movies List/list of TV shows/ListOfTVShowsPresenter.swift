//
//  ListOfTVShowsPresenter.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import UIKit

class ListOfTVShowsPresenter: ListOfTVShowsPresenterProtocol, ListOfTVShowsInteractorOutputProtocol {
    
    //MARK: - Properties
    weak var view: ListOfTVShowsViewProtocol?
    private let interactor: ListOfTVShowsInteractorInputProtocol
    private var router: ListOfTVShowsRouterProtocol 
    private var resultsListOfTVShows = [ResultsListOfTVShowsModel]()
    var numberOfRows: Int {
        return resultsListOfTVShows.count
    }
    
    init(view: ListOfTVShowsViewProtocol, interactor: ListOfTVShowsInteractorInputProtocol, router: ListOfTVShowsRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    
    func viewDidLoad() {
        view?.showLoadingIndicator()
        interactor.getdiscoverTV()
        isFavoriteNotificationListener()
    }
    
    func fetchedSuccessfuly(resultsListOfTVShows: [ResultsListOfTVShowsModel]) {
      let resultsListOfTVShowsSorted = resultsListOfTVShows.sorted(by: { $0.vote_count ?? 0 > $1.vote_count ?? 0 })
        self.resultsListOfTVShows = resultsListOfTVShowsSorted
        view?.hideLoadingIndicator()
        view?.reloadData()
    }
    
    func fetchingFailed(withError error: String) { 
         view?.hideLoadingIndicator()
         view?.showMessage(message: error)
    }
    
    
    
    func configureCell(cell: ListOfTVShowsCellViewProtocol, indexPath: IndexPath) {
        guard let data = self.resultsListOfTVShows.getElement(at: indexPath.row) else { return }
        let viewModel = ResultsListOfTVShowsViewModel(data: data)
        cell.configureCell(viewModel: viewModel, indexPath: indexPath)
    }
    
    func didTappedOnTVRow(indexPath: IndexPath) -> UIViewController {
         let data = resultsListOfTVShows.getElement(at: indexPath.row)
        return router.goToShowTVDetails(tvId: data?.id ?? 0, isFavorite: data?.isFavourite ?? false)
    }
    
    func didTappedFavorite(_ row: Int, _ isFavourite: Bool) {
        self.resultsListOfTVShows.getElement(at:row)?.isFavourite = isFavourite
        addToDB(row)
        view?.reloadData()
    }
    
    func getIsFavorite(_ row: Int) -> Bool {
        guard let isFavorite = self.resultsListOfTVShows.getElement(at: row)?.isFavourite else { return false }
        return isFavorite
    }
    
    func favoriteDatabaseUpdated(isFavourite : Bool, id: Int) {
        guard let index = resultsListOfTVShows.firstIndex(where: {$0.id == id}) else {return}
        self.resultsListOfTVShows.getElement(at: index)?.isFavourite = isFavourite
        view?.reloadData()
    }
    
    func addToDB(_ row: Int){
        guard let data = self.resultsListOfTVShows.getElement(at: row) else {return }
        let favouritesViewModel = FavouritesViewModel(id: data.id ?? 0, name: data.name ?? "", voteAverage: "\(data.vote_average ?? 0)", voteCount: "\(data.vote_count ?? 0)", firstAirDate: data.first_air_date ?? "", posterPath: data.poster_path ?? "")
        DatabaseManager.shared.add(favourite: favouritesViewModel)
    }
    
    fileprivate func isFavoriteNotificationListener() {
        _ = Notification.Name.favouritesUpdated.onPost { [weak self] (notification) in
            guard let self = self else {return}
            if let dict = notification.userInfo as NSDictionary? {
              if let favorite = dict["favorite"] as? Bool{
                if let id = dict["id"] as? Int {
                    self.favoriteDatabaseUpdated(isFavourite: favorite, id: id)
                  }
               }
             }
        }
    }
    
    func goToMyFavourites() -> UIViewController {
        return router.goToMyFavourites()
    }
     
}
