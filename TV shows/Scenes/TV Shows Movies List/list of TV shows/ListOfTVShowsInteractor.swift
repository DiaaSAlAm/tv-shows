//
//  ListOfTVShowsInteractor.swift
//  TV shows
//
//  Created by mac on 28/11/2020.
//

import Foundation

class ListOfTVShowsInteractor: ListOfTVShowsInteractorInputProtocol {
    
    //MARK: - Properties
    weak var presenter: ListOfTVShowsInteractorOutputProtocol?
    private let tvShowsListNetwork: TVShowsAPIProtocol = TVShowsAPI()
    
    func getdiscoverTV() {
        tvShowsListNetwork.getdiscoverTV { [weak self] (result) in
            guard let self = self else {return}
            switch result {
            case .success(let response):
                switch response {
                case nil:
                    self.presenter?.fetchingFailed(withError: response?.message ?? ConstantsMessage.genericErrorMessage)
                default:
                        self.presenter?.fetchedSuccessfuly(resultsListOfTVShows: response?.results ?? [])
                }
            case .failure(let error):
                print(error.localizedDescription)
                self.presenter?.fetchingFailed(withError:ConstantsMessage.genericErrorMessage)
            }
        }
    }
    
}
